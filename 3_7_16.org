* History Notes <2016-03-07 Mon>
* Review Session
** East India Company
It was a Royally chartered company of merchants. It was a backdoor into
collinization for the British.
** Sepoy Revolt
Indians fighting under British command. A revolt takes place in 19th century
due to cultural issues with cartiages. They did not remove the cartiages with
their teeth. These cartiages coated in stuff against their religious
views. Very devistating.
** Great Trek
Trek of Boers (Dutch in India) from conflict relating to British and
Dutch. When the British start to establish their control the dutch move inland
to avoid conflict.
** Boer War
Conflict between Dutch adn British due to the discovery of gold and diamonds
in Dutch (Boer) terriotory.
** Cecil Rhodes
Major British propentent of the Boer War and Indian collinization.
** Force Publique (FP)
Troops from tribes that hate each other. Europeans as officers. King Leopolds
army to control his terriotory in the Congo.
** Bosnia and Herzegovina
Very important geographically. It was in Saranoavo, Bosnia that ArchDuke Franz
Ferdinand II was assisinated. Nationalism big here among the baltic peoples.
** Bismarckian system
System of dioplomacy that was established by Bismarck. Its surface purpose was
to make Germany look like a peace maker. Real purpose was to fortify Germany
in case of attack of Germany's enemy (France). Problem this escalates
conflict. Bismarks successors failed to renew alliance with Russia. Russia
then scooped up by France. War between Austria and Serbia causes WWI.
** Triple Alliance
Germany, Austria, Italy.
** Triple Entente
France, England, Russia.
** Schlieffen Plan
A plan to avoid fighting a war on two fronts simulataneously. Devised
in 1905. Knock France out in weeks and then focus on Russia who would be
slower to mobilize. Would take France by suprise by violating the neutraility
of Belgium and invade them from the north instead of the east.
** trench warfare
War of attrition. Success measure in yards. You had a 19th century offense
(rifles, bayonettes) and a 20th century defense (machine guns).
** Battle of the Marne
The first time that WWI bogs down into the trenches.
** Battle of Verdun
Attempt to break the stalemate. German approach to try to bled France
dry. This does not work but is massively destructive.
** Gallipoli
Brainchild of Churchill. Create a diversion against the Germans by starting a
new front in the Ottoman empire at Gallipoli. Huge failure on for the British
and French.
** T.E. Lawerence
Stired up arab nationalism to hurt the cohesion of the Ottoman empire so they
could not help Germany in the war.
** Treaty of Versailles
Incredibly important because ended WWI and setup WWII. Germany and allies had
to pay huge reperations payments. In the War Guilt clause of the treaty
Germany blammed for the war. Germany lost Allasce and Loraine. Germany lost
their collonial possessions. Germany lost Poland. Thirdly, Germany was
demilitarized. Specifically demilitarized the Rhineland which was a buffer
between France and Germany. These terms were so harsh that it lead to the rise
of Hitler.
** Nicolas II and Alexandra
1917 big year of revolution in Russia. These people were the traditional
monarchs of Russia. In trouble becuase they were reluctant to reform. This
govt. assosciated with failure WWI pain and suffering. Also assosciated with
corruption, inefficiency, and Rasputin.
** Rasputin
"Snake oil" salesman that abused Alexandra and her sick son to get
govt. positions for him and his cronies.
** Kerensky
Ruler after the May (middle class) revolution. Ruler of the Provisional
govt. made a horrible decision to not pull out of the war. This lead to the
Bolshevik revolution.
** Lenin
Leader of the Bolshevik party in the Russia. Using the soviets, he came to
power. Using the Soviets, him and Trotsky adn Stalin pull a cout de te. He
pulled out of WWI.
** Trotsky
** Stalin
** Bolshevik Revolution
Huge civil war after this of Red - Bolskeviks vs Whites - Non-Bolskeviks.
** soviets
Workers councils in various cities of Russia. The main one in St. Petersburg.
** War Communism
Bolsheviks starts communism in conquered areas.
** New Economic Policy (NEP)
Lenin uses this to introduce temporary capitalism and the profit incentive to
jump start the Russia economy.
** "Socialism in One Country"
Stalin was the winner of the bid of ruling Russia after Lenin dies. He
believes in Socialism in One Country. That is implement marxism in home
country then expand.
** Permanent Revolution
Trotsky's view on how the Marxist revolution should take place. He believed in
exporting the revolution immediately elsewhere.
** Five Year Plans
Go into effect in 1928-1929. These plans where to industrialize Russia. Stalin
gets this industrialization by setting these huge quotas and working Russians
big time. By the eve of WWII Russia is the 3rd largest industrial
power. Stalin also uses these plans to solidify communism (IE repeal the
NEP). It involved collectivization.
** collectivization
Taking property from the Russain peasants and put them on group farms.
** kulaks
Upper class peasants that Stalin eliminated.
** Politburo
Russian govt. agency that brainstormed ideas. Subcommitee of the Central Committee.
** Hitler
** Weimer Republic
Germany comes out of WWI with this republic. The last days of WWI leads to a
revolution inside Germany that gets rid of the old monarchy and is replaced by
a democracy with a president (7 year term) and a Reichstag
(Paralament). Almost doomed from beginning due to the crises after the war
mainly economic.
** Occupation of the Ruhr
Territory in Germany that had a lot of natural resources. When Germany fell
behind on reparation payments, French occupies this area. Germans went on
strike. Germany supported these Germans by printing more money. This lead to hyper-inflation.
** fascism
Very different from communism; however, they both produce totalitarian
states. Very nationalistic in nature. At times, very rascist. It tends to be a
middle-class ideology. It total disagrees with Laize-faire. Govt. regulation
but industry still privately owned.
** Great Depression
1929. Furthers the rise of fascism. Becuase fo the inflation in Germany and
great depression. 43% unemployment in Germany.
** Beer Hall Putsch
Puts nazis on the radar. However, failed nazi revolution.
** Mein Kampf
Personal ranting of Hitler while in prision.
** Reichstag
German paralament. Nazis came to power through being elected into here. A
problem in the Reichstag was the presence of so many conflicting parties
(Nothing could be done).
** Goebels
** Enabling Act
When there is a fire in the Reichstag. Hitler uses this to become the dictator
of Germany for 4 years. This was a so-called temporary emergence measure. He
then embarks on some infamous policies. Eliminates all parties except the nazi
party and passes anti-semitic laws.
** anti-semitism
Neurember laws 1935 - segregate Jews from German society, deprive them of
power, ect. Results a good many Jewish families leaving Germany.
** Blitzkrieg
"Lighting war" - type of fighting Germany uses in WWII. Puts Germans back to
work building the German army. This is going to be a war of movement by using
planes (Luftwaffe), tanks, paratroopers, etc.
** appeasement
A policy of Britain and France towards Hitler in the 1930s. Britain is the
architec. France did not want to go along with it but had to. Essential if
Hitler's request/actions were justifiable, then do nothing. Inspired by using
Germany as a buffer to Russia, British guilt, etc.
** Anschluss
Germany takes Austria.
** Sudentenland
After a meeting in Munich takes Sudentenland.
** Lebensraum
Rational bring Germans together with Germans and more space for a rapidly
growing Germany.
** Vichy France
Lower 2/5 of France that went along with Germany to prevent war in southern
France. This govt. was colloborationists.
** Battle of Britain
Fall 1940 - Hitler decides to take on Britain. He had to destroy the RAF
(British airforce). This was so he could conduct a invasion of Britain. RAF
repeled the Germans.
** Winston Churchill
Wartime leader of Britain. He became an emotional leader during the war.
** Eastern Front
War between Russia and Germany when Germany turns against Russia in 1941.
** Pearl Harbor
** Holocaust
** New Order
What Hitler hoped to create throught the Holocaust.
