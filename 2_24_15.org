* History Notes <2016-02-24 Wed>
* Russian Revolution
** Russia on the Eve of the Revolution
We spent a lot of our time on the French Revolution as our model of revolution
but today we will put our focus on the Russian Revolution. It does not start
as a marxist revolution but becomes that. Like the French Rev. this revoluton
ramps up as social order refuses. In Russia this rev. starts as a middle class
revolution and becomes a proletariate revolution.

Russia from about 1880 on was in a whirlwind of change. These changes were
brought about by a late industrialization. When it does it happens very
rapidly. This is significant because it produces a certain amount of
dislocation and awkwardness. The middle class in Russia froze and becomes more
prosperious because of this. Industrialization will not be complete when this
revolution happens. It generates more wealth for the middle class and adds
some to its number. This middle class is going to want a role in the
governance of Russia.

80% of the Russian popultation is overwhelming peasant on the eve of the
revolution. Their is one thing a peasant wants is their own land. Serfdom
ended late in Russia in 1861. Secondly, when they were released from serfdom
they still did not get the land they wanted. Stolypin attempted to remedy in
part by land reform. This was before the rev. This only helped some. Many
ended up working for the Mir, a collective community farm owned by a village.

Russia is still very much dominated by the aristochracy. It is not interested
in reliquenshing control either.

Then comes WWI. Russia is part of the Triple Entaunt (Britian and
France). This is a miserable experience in Russia. It goes very badly in
Russia. It loses about half of its 15 million person army to death, desertion,
etc. This becomes a major political issue. It is going so badly that the Czar
Nicholas Romanovs (Nicholas II - wife Alexandra) leaves and goes to the front itself to try to
fix things. Removes him from politics and leaves his wife and advisors in
charge. This causes the civilian population to suffer. Railroads destroyed,
etc. One of these net effects is that Russians went without food. It created
inflation. Government tried to deal with some of these problems by printing
more money and this cause inflation.

The government consitied of a monarchy and a representive assembly know as the
Duma. Nicolas did not like the Duma. He came very close to abolishing it at
one point. Government in the popular mind became more and more associated with
corruption and incompentence. It becomes severly worse because of
Rasputin. Nicholas's son suffers from hemolphila. Rasputin was a so called
"healer" and hired by Nicolas and Alexandra to heal their son. While Nicolas
is away Rasputin uses his sway over Alexandra to install himself and his
friends in government offices. These most probable "con"men create a lot of
criticizism in the mind of the common people.
** March Revolution
In 1917(Know this date), revolution takes place in Russia. These are so bad on
March 8, strike break out in the royal city of St. Petersburg. In fact, women
start this because of food riots. When food has become to expensive to afford
people start rioting. They are just simply protesting. This usually happens
with women because they take care of the family. This is how the March
rev. starts.

By nighttime, their is around 200,000 people in the streets. This is the
beginning of the March Rev. Nicolas has troops there to management. He
abolishes the Duma becuase they were sympathetic. This happens in about three
days. Finally the army takes a vote in the baracks and vote not to fire on the
crowd. This implies they are sympathetic to the cause. With this the Czar has
lost St. Petersburg. March 12th he abdicates. Then a provisional govt. will be
formed. This govt. is the old Duma. This govt. is essentially the middle
class. The values of the provisional govt. are those of the middle class. They
choose Kerensky as their prime minister. They are trying to construct a new
govt. for Russia, finalize the rev. and deal with the incrediable misery cause
by the war. They are still in the war. They opt. to stay in the war and is not
popular (becomes part of there undoing).

Forming at the same time are the soviets. Soviets are workers
councils. Councils that represent the proleteariate. They are overtime
becoming a shadow (parallel) govt. They are more radical than the provisional
govt. Their are three parties that are socialist in nature and marxist
inspired. The most famous the Bolsheviks.
** Bolshevik Revolution
Lenin will be the leader of Bolsheviks. He is the brilliant mind behind the
Bolshevik rev. He has been living in excile and imprisoned in Syberia due to
politics the Czar didn't like. A foreign power (Germany) helps him return to
Russia after the March Revolution. Germany hopes that because of this
revolution Russia will pull out of the war. Lenin is a marxist theorist. He
has to mold marxist ideology a bit since it is not working the best in
practice. The problem Lenin has is that capitalism had not matured in Russia
enough to have the proletariate rise against the middle class. Lenin does not
want to wait so he just goes for it. He believed that sometimes it is the role
of the intellectuals in a movement to sometimes fast forward the process. He
dos this through indoctrination. What he and his two right hand men (Trotsky
and Stalin), work together to overthrow the provisional govt.

They campaigned with the proletariate that they would bring peace (removal
from war), land (land reform to satisfy peasants), and bread (no one goes
hungry). They also talked about confiscating industry and nationalize it. IE
govt possesion of industry. They work through those soviets to come to
power. They work to take over the big St. Petersburg soviet. In Novemember
through a cout they take over St. Petersburg and the St. Petersburg
soviet. The Novemember rev. is a marxist "Bolshevik" Rev.

What follows is a civil war in Russia. The Red (Bolsheviks) vs the Whites
(Middle class, aristocarchy, foreign influence, and anyone against
revolution). These foreign powers are Britian, France, and the United
States. There is a fear of marxism and they want a distraction in
Germany. This goes onto 1922 when the Bolseviks win. He tries to create war
communism. IE carry out his plans during the civil war.
** Lenin's Russia
After the war, Russia is such a mess economically that he decides to try to
jump start the economy with the New Economic Policy(NEP). It comes in 1921. It
is an interesting phenomion. He tries to use the profit incentive to try to
grow economically. Peasant get to own land, produce for market, and keep the
profit. The heavy industry stays in possession of the state. However, light
industry returned to private hands for a short time. This works.

The consitution the govt. puts in place has no reference to the communist
party. However, the communist party dominates politics. It becomes a modern
totalitarian state (one party politics). The Central Committee plans and
institutes policies. A subcommit of this is the Politburo. This is the think
tank of the Central Committee.

Modern states begin to use a lot of propaganda to persuade the citizenry of
political, etc. ideas. Totalitarian states really began to embrace this art
form through posters. (Image based bonus: Captialism is fat and well fed
extravagance, while citizenry average size and relatable and heroically) May
Day (1920) was the date of the Festival of Labor. The worker in propaganda
portrayed as heroes. 
