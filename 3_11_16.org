* History Notes <2016-03-11 Fri>
* WWII: The Struggle to End It
** Catching up from last time
Last time we talked about WWII. Hitler had just occupied the European
content. Only one major European power not under his control, Britain. He
tried to invaded Britain, but failed. The US is still not part of the war.
** US Isolationism
The US did not want to get involved in another global war. Nevertheless, the
US would have to play a role in this war. President Roosevelt provided money
and material to the British. FDR begins to expand the military and
reinstitutes the draft in 1940. He knew that if anything happened to Britain
that the US would have to act.
** Eastern Front
June of 1941, Hitler decides to invaded the Soviet Union. Germany's
relationship with the Soviet Union was of a non-agression pact. They also
planned to divy up eastern Europe.

Stalin was stunned by this. This first week of the invasion Stalin hid for a
couple of days to recover from this shocked event. Stalin did not believe
others that said Germany would invade Russia. He did not think the Germans
would not want a war on two fronts. He also was a little suscipicious of the
US and the British.

This is were the war in Europe against the Germans were won. What happens is
that Hitler puts together an army of 3 million soldiers to invade the Soviet
Union. They proceded into Russia in a broad front and not a straight line
(2,000 mile front). This goes badly for the Germans. The Russians retreated
before the German advance was a reason why Germany got so far into Russia.

In the midst of this, Stalin begins to relocate people to East Russia. He also
moved factories, etc. by trains. These factories then set backup and start
kranking out war material. This is a large undertaking.

Early on there was the seige of Leningrad. The objective of a seige was to
starve out the enemy. This lasted for 16 months (also called the 1,000 days
seige). Around 3 million people dies. Kiev fell early. Many military
historians will say that the Battle of Stalingrad was the largest military
battle in history. It was fought building by building, room by room. This was
a turning point in WWII. Snipers were important on both sides in this fight.

Ultimatly by 1942, the Russians are begining to gradulally drive out the
Russians. Germans also like Napolean got caught in the Russian winter. This
winter was the worst on record in approx. 100 years. Germans only had summer uniforms.
** Grand Alliance: Britain, USSR, and US
In the Dec. 1941, Japan attacked Pearl Harbor. Because Japan was an alliance
of Germany, the US enters the war. This creates the Grand Alliance. There
strategy was to win in Europe first then win in Japan. The plan was for the
Russians to invade from the East. The US, Canada, Britain was to invade from
France from the West. This made a war on two fronts. Stalin wanted help by
opening up another front.
** D-Day (Operation Overlord)
This was a battle plan to transport massive amounts of troops and invade
France from Normandy. They would then liberate France and then head towards
Berlin. D-Day (June 6, 1944) proceeded in bad weather. They farried 2 million
men amphibiously across the English Channel into Normandy. This was not where
the Germans expected to invade from. When close to shore, the troops waded to
shore from their amphibious vessels. They encountered the German
response. This was carnage. This invasion took weeks. They finally fought
their way out of the beachhead. They then go liberate Paris. They then
proceeded east towards Germany. Things were not the smoothest to make it to
Berlin.
** Battle of the Bulge
Germany is a mess by this point. Hitler pulls it together to launch a major
counter-offensive at the Battle of the Bulge. This derails the Allied advance
for a while.
** Bombing Campagins
The British and the Americans had bombed all major German cities to
rubble. This has been going on for a while. This is a incredible destruction
on the German infrastructure. The famous fire bombing of Dresden. Dresden was
a jewel in art and architecture. This is the allies way to break German
moral. In a couple of days, allied incedeary bombs cause incredible
destruction. Why did a lot of people die here? Their was a lot of German
refuges in Dresden. The decision to bomb Dresden was very contriversial. This
bombing was purely to break German moral.

Even before the Dresden bombing their were officers in the German army that
wanted to stop Hitler.
** Attempts to assassinate Hitler - July 1944
Failed attempt to bomb and kill Hitler by German officers.
** War in the Pacific: Hiroshima & Nagasaki
Japanese imperialism in the pacific cause the war in Japan to be spread
out. This led to an island hopping campaign. Japan as it expanded butted heads
with the US. The US started a trade embargo in Japan. The Japanese response
was to bomb Pearl Harbor (Dec. 7, 1941).

The US strategy in the Pacific was to hold the line as long as possible while
they defeated Hitler. The fighting in the Pacific was awful. To be drafted and
sent to the Pacific was a death sentence.

The decision to drop the atomic bomb is still a contraversial decision to this
day. They had developed the bomb so they could develope it before the Germans
did. The US argued that this would save more lives. They felt confidantly that
the Japanese would fight to the last bit in regular warfare. It was first
dropped in Hiroshima and then later Nagasaki. Another reason to use the atomic
bomb was that the Allies had allready bombed major Japanese cities and they
refused to surrender.
** Some Effects of WWII
WWII was called a watershed event. An event into which history flowed into and
out of. It shaped the world that we currently live in. Around 50 million
people died. The Russians had the highest casuality loss. China had the second
highest casuality loss.

The economic effects were major. The US did well in this war through the war
industry. It has been argued that this brought the US out the of the Great
Depression. However, Europe and Asia was destroyed in this war. However, they
recovered rather quickly.

One of the major consequences of WWII, was the homeless and refugees. What to
do about this? Dealing with their future took years after the war.

Technology advanced greatly during this war. Radar, atomic energy (bombs),
etc.

The Cold War is rooted in the end of WWII.

Finally, decolonization comes about as the fighting was not only in Europe but
in colonies. Colonial populations recruited for war. These populations
wondered if they had been fighting for their colonial masters freedom, why did
they not get freedom. African veterans did not get the benefits of British
veterans, etc.
