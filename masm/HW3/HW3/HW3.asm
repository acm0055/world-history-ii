TITLE Homework 3
INCLUDE Irvine32.inc
;author: Austin Chase Minor
;version: 2/3/2016

.386
.model flat, stdcall

.data
message BYTE "Go Auburn", 0

.stack 4096

ExitProcess PROTO, dwExitCode:DWORD

.code
main PROC
	mov edx, offset message
	Call WriteString
	INVOKE ExitProcess, 0
main ENDP
END main