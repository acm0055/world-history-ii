* Proofs
** Loop Invariants
*** Initialization
**** Before the start of iteration of the look with j = 2,
the subarray A[1...1] consists of the elements originally in
A[1...1] but in sorted order.
**** Trivially true because A[1...1] is a one-element array which is by definition sorted!
*** Maintenance
**** Suppose that before the start of the loop's iteration with value j, the subarray A[1...j-1]
consists of the elements originally in A[1...j-1] but in sorted order.
**** During the jth iteration of the loop, the while loop (lines 4- 6) compares A[j] with a[j-1], A[j-2] etc. until
(i) either a number A[k] <= A[j] is found or (ii) it turns out that all
numbers A[j-1]...A[\2] A[\1] are greater that A[j]
**** Each number found to be greater than A[j] is moved one cell to the right, i.e., if
A[j-1]>A[j] then A[j-1] is moved to A[j] and so on.
**** If case (i) holds, then the numbers A[k+1}, ..., A[j-1] are moved to cells A[k+2}....A[j] respectively and A[j]
is placed (line 8) in A[k+1]
**** Since in this case A[\1]...A[k] are not moved, by assumption (1) these are in the sorted order.
**** By the same assumption, A[k+1]...A[j-1] were in sorted order, so now A[k+2],...A[j] are in sorted order
**** Since A[k]<=A[j] and A[j] is now in A[k+1], A[k]<=A[k+1]
**** Since A[k] is the first number found by the while loop such that A[k]<=A[j], we know that A[k+1} must
have been greater than A[j]. So now A[k+1] < A[k+2]
**** Last two steps imply that A[k], A[k+1] and A[k+2] are in sorted order. 
**** 5,6,& 9 imply that A[\1]...A[j} are in sorted order at the end of current iteration.
**** Similarly for (ii)
*** Termination
**** Look at sides
*** Reading Assignments
**** Ch.2 Section 2.1
**** Ch.2 Section 2.3
**** 2.1-3, p.22
** Inductive Proofs
*** General
**** Base Case: Prove for the base case(s)
**** Inductive Hypothesis: Assume true for the first k-1 items
**** Inductive step: Show that it holds for the kth item
**** Inductive hypothesis essential generate a valid equation
*** Example Fib nums
**** In hand notes
*** Alogorithm correctness
****  In pdf
*** Find-Max
**** In pdf
* Summary 
** How to prove incorrect
*** Try
**** Counterexample
**** Proof by contradiction
** How to prove correct
*** Try
**** Proof by contradiction or loop invariants
***** Mainly for iterative
**** Proof by induction
***** Mainly for recursive
