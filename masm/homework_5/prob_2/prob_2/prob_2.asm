TITLE Addressing Modes			(main.asm)
INCLUDE Irvine32.inc

.data
alpha 	DWORD	11223344h, 55667788h  
beta 	DWORD	99AABBCCh, 0DDEEFF00h
gamma  	DWORD 	1234h

.code		
main PROC	
	mov eax, 1234h;			Immediate
	mov ecx, eax;			Register to Register	
	mov edi, OFFSET beta;		Immediate	
	mov [gamma], eax; 		Indirect	
	mov esi, [gamma];			Direct	
	mov esi, 4;  			Immediate	
	mov eax, beta[esi];		Indirect-offset	
	mov ebx, OFFSET alpha;		Immediate	
	mov eax, [ebx];  			Indirect	
	mov eax, 4[ebx]; 			Indirect-displacement	
	mov eax, 4[ebx][esi];  		Base-Indirect-displacement	
exit
main ENDP
END main