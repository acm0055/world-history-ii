TITLE Homework 4 Problem 4
INCLUDE Irvine32.inc
;author: Austin Chase Minor
;version: 2/22/2016

.386
.model flat, stdcall

.stack 4096

.data
userMsg BYTE "Enter an integer n: ", 0

ExitProcess PROTO, dwExitCode:DWORD

.code
main PROC
	mov edx, OFFSET userMsg
	call WriteString
	call ReadInt
	;;Read user input and calculate loop length
	mov ecx, eax
	add eax, eax
	dec eax
	mov ebx, 0
	L1:
		add ebx, eax
		sub eax, 2
		loop L1

	mov eax, ebx
	call WriteInt
	INVOKE ExitProcess, 0
main ENDP
END main