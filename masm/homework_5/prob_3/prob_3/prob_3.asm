TITLE Homework 5 Problem 3
INCLUDE Irvine32.inc
;author: Austin Chase Minor
;version: 2/22/2016

.386
.model flat, stdcall

.stack 4096

ExitProcess PROTO, dwExitCode:DWORD

.data
Array WORD 11h, 22h, 33h, 44h, 55h, 66h, 77h,
88h, 99h
ConstVal WORD 05h
ModArray WORD 9 DUP(?)

.code
main PROC
	mov ecx, LENGTHOF Array
	mov esi, 0
	L1:
		mov ax, [Array + esi]
		sub ax, ConstVal
		mov [ModArray + esi], ax
		add esi, TYPE Array
	loop L1
	INVOKE ExitProcess, 0
main ENDP
END main