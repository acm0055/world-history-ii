TITLE Homework 3 Problem 4
INCLUDE Irvine32.inc
;author: Austin Chase Minor
;version: 2/3/2016

.386
.model flat, stdcall

.stack 4096

.data
var1 SWORD 1
var2 SWORD 2
var3 SWORD 4

ExitProcess PROTO, dwExitCode:DWORD

.code
main PROC
	mov ax, var1
	add ax, var2
	add ax, var3
	INVOKE ExitProcess, 0
main ENDP
END main