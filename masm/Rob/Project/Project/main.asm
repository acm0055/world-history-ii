; AddVariables.asm 
; Robert Lagen
; 2/4/2016

.386
.model flat,stdcall
.stack 4096

.data
firstval	SWORD 1
secondval	SWORD 2
thirdval	SWORD 3
sum			SWORD 0

.code
main PROC
	mov ax,firstval
	add ax,secondval
	add ax,thirdval
	mov sum,ax

main ENDP

END main