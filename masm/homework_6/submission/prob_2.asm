;;
;; Author: Austin Chase Minor
;; Version: 12-3-2016
;; Problem 2: Homework 6
;;

INCLUDE Irvine32.inc

.386

.model flat, stdcall

.stack 4096
	
.data
Prompt1 BYTE "Please input a value: ", 0
Spring WORD 10 DUP(?)	

ExitProcess PROTO, dwExitCode:DWORD

.code
main PROC
	mov ecx, 10		; Number of inputs from user
	mov esi, 0		;index for array
	mov edx, OFFSET Prompt1 ; Set display for message
L1:
	call WriteString		; Display Prompt1
	call ReadInt			; Read User Input
	mov Spring[esi], ax
	add esi, TYPE Spring
	loop L1

	mov ecx, 10 		; Number of inputs from user
	mov esi, 0 		; index for array

L2:				;Write out user input
	;; Print out array value (Signed)
	movsx eax, Spring[esi]
	call WriteInt
	call crlf

	;; Increment array index
	add esi, TYPE Spring
	loop L2
invoke ExitProcess, 0
main ENDP
END main
