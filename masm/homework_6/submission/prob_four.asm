;;
;; Author: Austin Chase Minor
;; Version: 12-3-2016
;; Problem 4: Homework 6
;;

INCLUDE Irvine32.inc

.386

.model flat, stdcall

.stack 4096
	
.data
prompt1 BYTE "Set and clear the ", 0
flags BYTE "CF", 0, "OF", 0, "ZF", 0, "SF", 0
prompt2 BYTE " flag using ", 0
operation BYTE "add", 0, "sub", 0
prompt3 BYTE " operation.", 0
set BYTE "Set:", 0
clear BYTE "Clear:", 0

ExitProcess PROTO, dwExitCode:DWORD

.code
print PROC
	push ebx		; add/sub prompt
	push ecx		; flag prompt
		
	mov edx, OFFSET prompt1	;print intro
	call WriteString	;print intro
	mov edx, ecx 		;print flag type
	call WriteString	;print flag type
	mov edx, OFFSET prompt2 ;print prompt2
	call WriteString	;print prompt2
	mov edx, ebx 		;print add/sub prompt
	call WriteString	;print add/sub prompt
	mov edx, OFFSET prompt3 		;print prompt3
	call WriteString	;print prompt3
	call crlf
	
	pop ecx
	pop ebx

	ret
print ENDP

main PROC
;; Set and clear CF with add
	mov ecx, OFFSET flags	  ; carryflag prompt
	mov ebx, OFFSET operation ; add prompt
	call print

	mov edx, OFFSET set
	call WriteString
	mov cx, 0FFFFh
	add cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    add cx, 1
	call DumpRegs
	call crlf
	
;; Set and clear CF with sub
	mov ecx, OFFSET flags	      ; carryflag prompt
	mov ebx, OFFSET operation + 4 ; subtract prompt
	call print

	mov edx, OFFSET set
	call WriteString
	mov cx, 0000h
	sub cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    sub cx, 1
	call DumpRegs
	call crlf

;; Set and clear OF with add
	mov ecx, OFFSET flags + 3 ; carryflag prompt
	mov ebx, OFFSET operation ; add prompt
	call print

	mov edx, OFFSET set
	call WriteString
	mov cx, 7FFFh
	add cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    add cx, 1
	call DumpRegs
	call crlf
	
;; Set and clear OF with sub
	mov ecx, OFFSET flags + 3     ; carryflag prompt
	mov ebx, OFFSET operation + 4 ; subtract prompt
	call print
	
	mov edx, OFFSET set
	call WriteString
	mov cx, 8000h
	sub cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    sub cx, 1
	call DumpRegs
	call crlf

;; Set and clear ZF with add
	mov ecx, OFFSET flags + 6 ; carryflag prompt
	mov ebx, OFFSET operation ; add prompt
	call print
	
	mov edx, OFFSET set
	call WriteString
	mov cx, 0FFFFh
	add cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    add cx, 1
	call DumpRegs
	call crlf
	
;; Set and clear ZF with sub
	mov ecx, OFFSET flags + 6     ; carryflag prompt
	mov ebx, OFFSET operation + 4 ; subtract prompt
	call print
	
	mov edx, OFFSET set
	call WriteString
	mov cx, 1h
	sub cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    sub cx, 1
	call DumpRegs
	call crlf

;; Set and clear SF with add
	mov ecx, OFFSET flags + 9 ; carryflag prompt
	mov ebx, OFFSET operation ; add prompt
	call print
	
	mov edx, OFFSET set
	call WriteString
	mov cx, 0FFFEh
	add cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    add cx, 1
	call DumpRegs
	call crlf
	
;; Set and clear SF with sub
	mov ecx, OFFSET flags + 9     ; carryflag prompt
	mov ebx, OFFSET operation + 4 ; subtract prompt
	call print
	
	mov edx, OFFSET set
	call WriteString
	mov cx, 8001h
	sub cx, 1
	call DumpRegs
	call crlf

	mov edx, OFFSET clear
	call WriteString
    sub cx, 1
	call DumpRegs
	call crlf
	
	invoke ExitProcess, 0
main ENDP
END main
