;;
;; Author: Austin Chase Minor
;; Version: 12-3-2016
;; Problem 3: Homework 6
;;

INCLUDE Irvine32.inc

.386

.model flat, stdcall

.stack 4096
	
.data
prompt BYTE "Please input a value: ", 0
spacing BYTE ", ", 0
result BYTE "The minimum of value inputs is: ", 0
;Using 12 dup(?) gave me all zeros so using this to show that it works
;I even tried .data? but did not work
Spring WORD 2, 3, 1332, 123, 1234, 1, 90, 321, 90, 13, 3123, 1234

ExitProcess PROTO, dwExitCode:DWORD

.code

MinArray PROC
	pushad

	push esi		;store array pointer
	push ecx 		;store array size
	
	sub ecx, 1 		; Decrease loop count due to offset
	mov ax, WORD PTR [esi] ; store initial min
	add esi, 2 	; Offset array by one for algorithm

;; Find minimal
L1:
	cmp WORD PTR [esi], ax
	jnc L11			; jump if [esi] >= min
	mov ax, WORD PTR [esi]
L11:
	add esi, 2
	loop L1

;; Print Min
	movzx eax, ax 		;Make sure upper eax not full of bad data
	mov edx, OFFSET result
	call WriteString
	call WriteDec		;Write min
	call crlf

;; Subtract min from each element
	pop ecx 		; Restore Array Size
	pop esi			; Restore Array Pointer
	push esi 		; Save Array Pointer
	push ecx		; Save Array Size
L2:
	sub [esi], ax		; Subtract min
	add esi, 2
	
	loop L2

;; Print Array
	pop ecx			;Restore Array Size
	pop esi			;Restore Array Pointer
	
L3:
	movzx eax, WORD PTR [esi]
	call WriteDec
	mov edx, OFFSET spacing
	call WriteString
	
	add esi, 2
	loop L3
	
	popad

	ret
MinArray ENDP

main PROC
	mov ecx, LENGTHOF Spring
	mov esi, OFFSET Spring

	call MinArray
invoke ExitProcess, 0
main ENDP
END main
